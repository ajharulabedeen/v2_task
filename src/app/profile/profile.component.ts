import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../_services/token-storage.service';
import {Admission} from '../models/admission.model';
import {AdmissionService} from '../_services/admission.service';
import {Informations} from '../models/informations.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  form: any = {
    full_name: null,
    mobile: null,
    dept: null,
    ssc_point: null,
    hsc_point: null
  };

  isSignUpFailed = false;
  profileExist = false;
  errorMessage = '';

  full_name: string = '';
  mobile: string = '';
  dept: string = '';
  ssc_point: number = 0.0;
  hsc_point: number = 0.0;

  totalDueAmount = 0.0;
  currentUser: any;
  information = new Informations();

  constructor( private admissionService: AdmissionService, private token: TokenStorageService) { }

  ngOnInit(): void {
    this.currentUser = this.token.getUser();
    this.getAdmission();
  }

  getAdmission(): void{
    this.admissionService.getAdmission().subscribe(
      data => {
        this.full_name = data['full_name'];
        this.mobile = data['mobile'];
        this.dept = data['dept'];
        this.ssc_point = data['ssc_point'];
        this.hsc_point = data['hsc_point'];
        this.profileExist = true;
        this.totalDue();
      },
      err => {
        this.errorMessage = err.error.message;
      }
    );
  }

  totalDue(): void {
      if (this.dept === this.information.SWE){
          this.totalDueAmount = this.information.SWE_credit * this.information.per_credit;
      } else if (this.dept === this.information.CSE){
        this.totalDueAmount = this.information.CSE_credit * this.information.per_credit;
      }else {
        this.totalDueAmount = this.information.EEE_credit * this.information.per_credit;
      }
  }

  onSubmit(): void {
    let admission = new Admission(this.form.full_name, this.form.mobile,
      this.form.dept, this.form.ssc_point, this.form.hsc_point);

    console.log(admission);

    this.admissionService.admission(admission).subscribe(
      data => {
        console.log(data);
        this.getAdmission();
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }
}
