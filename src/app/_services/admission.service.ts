import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {TokenStorageService} from './token-storage.service';
import {Admission} from '../models/admission.model';
import {Observable} from 'rxjs';

const AUTH_API = 'http://localhost:8080/api/auth/admission';

@Injectable({
  providedIn: 'root'
})
export class AdmissionService {

  constructor(private http: HttpClient, private token: TokenStorageService) { }

   admission(admission: Admission): Observable<any> {
    let tokenAuth: any = ``;
    tokenAuth = this.token.getToken();
     const httpOptions = {
       headers: new HttpHeaders({ 'Content-Type': 'application/json',
         'X-Access-Token' : tokenAuth})
     };
     return this.http.post(AUTH_API, admission, httpOptions);
  }


  getAdmission(): Observable<any>{
    let tokenAuth: any = ``;
    tokenAuth = this.token.getToken();
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json',
        'X-Access-Token' : tokenAuth})
    };
    return this.http.get(AUTH_API, httpOptions);
  }
}
