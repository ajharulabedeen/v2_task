export class Admission {
  public full_name: string;
  public mobile: string;
  public dept: string;
  public ssc_point: number;
  public hsc_point: number;

  constructor(full_name: string,
              mobile: string,
              dept: string,
              ssc_point: number,
              hsc_point: number ) {
    this.full_name = full_name;
    this.mobile = mobile;
    this.dept = dept;
    this.ssc_point = ssc_point;
    this.hsc_point = hsc_point;
  }

}
