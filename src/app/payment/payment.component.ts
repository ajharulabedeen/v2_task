import { Component, OnInit } from '@angular/core';
import {AdmissionService} from '../_services/admission.service';
import {TokenStorageService} from '../_services/token-storage.service';
import {Informations} from '../models/informations.model';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {


  //refac: model
  full_name: string = '';
  mobile: string = '';
  dept: string = '';
  ssc_point: number = 0.0;
  hsc_point: number = 0.0;
  credit: number = 0.0;


  currentUser: any;
  form: any = {
    semester: null,
    cgpa: null
  };
  isSignUpFailed: boolean = false;
  errorMessage: any;
  totalDueAmount = 0.0;
  information = new Informations();


  constructor( private admissionService: AdmissionService, private token: TokenStorageService) { }

  ngOnInit(): void {
    this.currentUser = this.token.getUser();
    this.getAdmission();
  }

  onSubmit() {
    console.log(this.form.cgpa);
    console.log(this.form.semester);
    return false;
  }

  getAdmission(): void{
    this.admissionService.getAdmission().subscribe(
      data => {
        this.full_name = data['full_name'];
        this.mobile = data['mobile'];
        this.dept = data['dept'];
        this.ssc_point = data['ssc_point'];
        this.hsc_point = data['hsc_point'];
        this.totalDue();
      },
      err => {
        this.errorMessage = err.error.message;
      }
    );
  }

  // refac: util
  totalDue(): void {
    if (this.dept === this.information.SWE){
      this.totalDueAmount = this.information.SWE_credit * this.information.per_credit;
      this.credit = this.information.SWE_credit;
    } else if (this.dept === this.information.CSE){
      this.credit = this.information.CSE_credit;
      this.totalDueAmount = this.information.CSE_credit * this.information.per_credit;
    }else {
      this.credit = this.information.EEE_credit;
      this.totalDueAmount = this.information.EEE_credit * this.information.per_credit;
    }
  }


}
